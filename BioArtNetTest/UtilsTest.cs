using Xunit;
using static BioArtNet.Tools.CellExtension.Utils;

namespace BioArtNetTest
{
    public class UtilsTest
    {
        [Theory]
        [InlineData(12, 16, 12)]
        [InlineData(28, 16, 12)]
        [InlineData(-1, 16, 15)]
        [InlineData(-17, 16, 15)]
        [InlineData(-20, 16, 12)]
        public void LoopIndexTest(int i, int n, int result)
        {
            Assert.True(LoopIndex(i, n) == result);
        }

        [Theory]
        [InlineData(5, 0, 5, 0)]
        [InlineData(6, 0, 5, 1)]
        [InlineData(6, 1, 5, 2)]
        [InlineData(5, 1, 5, 1)]
        [InlineData(0, 1, 5, 4)]
        public void LoopIndexSETest(int i, int start, int end, int result)
        {
            Assert.Equal(result, LoopIndex(i, start, end));
        }
    }
}