using System;
using BioArtNet.Core;
using BioArtNet.Tools.CellExtension;

namespace BioArtNet.Example
{
    public class Cell : ICell<Entity>
    {
        public Entity Entity { get; set; }

        public bool IsWall { get; set; }
        
        public bool IsPassable 
            => (Entity == null) && (!IsWall);
        
        public void SetEntity(IEntity entity)
        {
            switch (entity)
            {
                case null:
                    Entity = null;
                    break;
                case Entity trueEntity:
                    Entity = trueEntity;
                    break;
                default:
                    throw new ArgumentException($"{nameof(entity)} must be of type {nameof(BioArtNet.Example.Entity)}, because it is a required crutch.");
            }
        }
    }
}
