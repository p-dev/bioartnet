using System.Collections.Generic;

namespace BioArtNet.Core
{
    /// <summary>
    /// Сущность.
    /// </summary>
    public interface IEntity
    {
        Dna Dna { get; }

        /// <summary>
        /// Стэк для чего-то там.
        /// </summary>
        Stack<byte> Stack { get; }
        
        /// <summary>
        /// Память для чего-то там.
        /// </summary>
        IDictionary<int, byte> Memory { get; }
    }
}