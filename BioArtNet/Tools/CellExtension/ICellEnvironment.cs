using BioArtNet.CellExtension;
using BioArtNet.Core;

namespace BioArtNet.Tools.CellExtension
{
    public interface ICellEnvironment<out TEntity, out TCell> : IEnvironment<TEntity>
        where TCell : ICell<IEntity>
        where TEntity : ICellEntity
    {
        TCell this[int i, int j] { get; }
        TCell this[Vector2 v] { get; }
    }
}