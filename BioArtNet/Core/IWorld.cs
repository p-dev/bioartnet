using System.Collections.Generic;

namespace BioArtNet.Core
{
    /// <summary>
    /// Мир. Соединяет в себе отдельные компоненты-интерфейсы, и позволяет запускать их и 
    /// работать с ними.
    /// </summary>
    /// <typeparam name="TEntity">Тип сущностей в мире</typeparam>
    /// <typeparam name="TEnvironment">Тип окружающей среди</typeparam>
    /// <typeparam name="TInterpreter">Тип интерпретатора</typeparam>
    public interface IWorld<out TEnvironment, out TInterpreter>
        where TInterpreter : IInterpreter<IEnvironment<IEntity>>
        where TEnvironment : IEnvironment<IEntity>
    {
        TEnvironment Environment { get; }

        TInterpreter Interpreter { get; }

        void Iterate();
        int Generation { get; }
    }
}