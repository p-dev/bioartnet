using System;
using System.IO;
using System.Linq;
using System.Net.Mime;
using BioArtNet.CellExtension;
using BioArtNet.Core;
using BioArtNet.Tools.CellExtension;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.ColorSpaces;
using SixLabors.ImageSharp.Formats.Gif;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace BioArtNet.Example
{
    public class Visualizer : IVisualizer<World>
    {
        public World World { get; }

        public string OutputDir { get; set; }

        public Visualizer(World world, string outputDir)
        {
            World = world;
            OutputDir = outputDir;
            Directory.GetFiles(OutputDir).ToList().ForEach(File.Delete);
        }
        
        public void Render()
        {
            var env = World.Environment;

            using (var image = new Image<Rgba32>(env.Width, env.Height))
            {
                for (var i = 0; i < env.Height; i++)
                for (var j = 0; j < env.Width; j++)
                {
                    if (env[j, i].HasEntity())
                    {
                        image[j, i] = Rgba32.Aqua;
                    }
                    else if (env[j, i].IsWall)
                    {
                        image[j, i] = Rgba32.Gray;
                    }
                    else
                    {
                        image[j, i] = Rgba32.Black;
                    }
                }

                using (var fs = new FileStream($"{OutputDir}/gen_{World.Generation}.png", FileMode.Create))
                    image.SaveAsPng(fs);
            }
        }

        public void GenGif()
        {
            using (var gifImage = new Image<Rgba32>(World.Environment.Width, World.Environment.Height))
            {
                var imageFiles = Directory.GetFiles(OutputDir);
                for (var i = 0; i < imageFiles.Length; i++)
                {
                    using (var frameImage = Image.Load(imageFiles[i]))
                    {
                        gifImage.Frames.InsertFrame(i, frameImage.Frames.RootFrame);
                    }
                }

                using (var fs = new FileStream($"{OutputDir}/gens.gif", FileMode.Create))
                    gifImage.SaveAsGif(fs);
            }
        }
    }
}