using System;
using BioArtNet.Tools.Commands;

namespace BioArtNet.Example
{
    public enum OpCode : byte
    {
        // Moving
        
        Idle = 0x00,
        Move = 0x01,
        MoveTo = 0x02,
        MoveToS = 0x03,
        Rotate = 0x04,
        RotateS = 0x05,
        
        // Division
        
        Divide = 0x06,
        DividePtr = 0x07,
        DividePtr2 = 0x08,
        
        // Conditional
        
        Goto = 0x10,
        Goto2 = 0x11,
        If = 0x12,
        If2 = 0x13, 
//        TODO: 
//        Switch = 0x14,
//        Switch2 = 0x15,

        // Arithmetic

        Add = 0x18,
        Sub = 0x19,
        Not = 0x1C,
        And = 0x1D,
        Or = 0x1E,
        Xor = 0x1F,
        
        // Stack and local
        
        Push = 0x20,
        Pop = 0x21,
        Dup = 0x22,
        Swap = 0x23,
        Load = 0x26,
        
        Store = 0x27,
        
        // Other
        
        CanMove = 0x30,
    }
}