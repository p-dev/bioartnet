using BioArtNet.Core;

namespace BioArtNet.Tools.CellExtension
{
    public interface ICellEntity : IEntity
    {
        Direction Orientation { get; set; }
    }
}