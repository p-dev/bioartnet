using System;
using BioArtNet.Core;
using BioArtNet.Tools.CellExtension;
using BioArtNet.Tools.Commands;

namespace BioArtNet.Example.Modules
{
    public class MoveModule : ModuleBase<CommandContext>
    {
        [Command(OpCode.Idle, true)]
        public void IdleCommand()
        {
            
        }
        
        [Command(OpCode.Move, true)]
        public void MoveCommand()
        {
            MoveToCommand((byte) Context.Entity.Orientation);
        }

        [Command(OpCode.MoveTo, true)]
        public void MoveToCommand(
            [ArgRange(0, 3, LimitingRule.Loop)] byte direction)
        {
            var dir = (Direction) direction;
            var dirV = dir.ToVector2();
            
            Context.Environment.TryMoveEntity(Context.EntityPosition, Context.EntityPosition + dirV);
        }

        [Command(OpCode.MoveToS, false)]
        public void MoveToSCommand()
        {
            MoveToCommand(Context.Entity.Stack.Pop());
        }

        [Command(OpCode.Rotate, false)]
        public void RotateCommand(
            [ArgRange(0, 3, LimitingRule.Loop)] byte direction)
        {
            var resOrientationVal = (direction + (byte) Context.Entity.Orientation) % Enum.GetNames(typeof(Direction)).Length;
            Context.Entity.Orientation = (Direction) resOrientationVal;
        }
        
        [Command(OpCode.RotateS, false)]
        public void RotateSCommand()
        {
            RotateCommand(Context.Entity.Stack.Pop());
        }

        [Command(OpCode.Divide, true)]
        public void DivideCommand()
        {
            DividePtr2Command(Context.Entity.Dna.CodePtr);
        }
        
        [Command(OpCode.DividePtr, true)]
        public void DividePtrCommand(byte cmdPtr)
        {
            DividePtr2Command(cmdPtr);
        }
        
        [Command(OpCode.DividePtr2, true)]
        public void DividePtr2Command(ushort cmdPtr)
        {
            var entity = Context.Entity;
            var newEntity = new Entity(entity.Dna.Mutate(0.1f));
            var entityPos = Context.EntityPosition;
            Context.Environment[entityPos + entity.Orientation.ToVector2()].Entity = newEntity;
        }
    }
}