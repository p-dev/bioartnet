namespace BioArtNet.Tools.CellExtension
{
    public enum Direction : byte
    {
        Up = 0,
        Right = 1,
        Down = 2,
        Left = 3,
    }
}