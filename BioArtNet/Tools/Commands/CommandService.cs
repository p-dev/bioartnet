using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BioArtNet.Commands;
using BioArtNet.Core;
using BioArtNet.Example;
using BioArtNet.Tools.CellExtension;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace BioArtNet.Tools.Commands
{
    public class CommandService<TCommandContext> : ICommandService<TCommandContext>
        where TCommandContext : ICommandContext<IEntity>
    {
        public IEnumerable<ModuleBase<TCommandContext>> Modules { get; }

        private readonly Dictionary<int, CommandInfo<TCommandContext>> _commands;
        public IReadOnlyDictionary<int, CommandInfo<TCommandContext>> Commands => new Dictionary<int, CommandInfo<TCommandContext>>(_commands);

        private readonly ILogger _logger;
        
        public CommandService(IServiceProvider services, ILogger<CommandService<TCommandContext>> logger)
        {
            _logger = logger;
            
            // Resolve all modules in IoC container
            Modules = services.GetServices<ModuleBase<TCommandContext>>();
            
            _commands = new Dictionary<int, CommandInfo<TCommandContext>>();
            foreach (var module in Modules)
            {
                foreach (var methodInfo in module.GetType().GetMethods())
                {
                    var attribute = methodInfo.GetCustomAttribute<CommandAttribute>();
                    if (attribute == null) continue;

                    _commands[attribute.OpCode] = new CommandInfo<TCommandContext>(methodInfo, module, attribute);
//                    methodInfo.GetParameters()[0].
                }
            }
        }

        public void InvokeCommand(ushort opCode, TCommandContext context)
        {
            var commandInfo = Commands[opCode];
            
            commandInfo.ModuleInst.Context = context;
            var dna = context.Entity.Dna;
            
            var args = commandInfo.ArgInfos.Select(arg =>
            {
                var type = arg.Type;
                
                object res;
                
                if (type == typeof(byte))
                    res = dna.GetByte();
                else if (type == typeof(sbyte))
                    res = dna.GetSByte();
                else if (type == typeof(short))
                    res = dna.GetShort();
                else if (type == typeof(ushort))
                    res = dna.GetUShort();
                else if (type == typeof(int))
                    res = dna.GetInt();
                else if (type == typeof(uint))
                    res = dna.GetUInt();
                else if (type == typeof(long))
                    res = dna.GetLong();
                else if (type == typeof(ulong))
                    res = dna.GetULong();
                else
                    throw new ArgumentException("The type of the command argument must be a primitive type.");

                if (arg.ArgRange != null)
                {
                    var argRange = arg.ArgRange;
                    if (!argRange.IsValid(res))
                    {
                        switch (argRange.LimitingRule)
                        {
                            case LimitingRule.Ignore:
                                return null;
                            
                            case LimitingRule.DefaultValue:
                                return Activator.CreateInstance(type);
                            
                            case LimitingRule.Constrain:
                                if (Comparer.Default.Compare(res, argRange.Minimum) >= 0) return argRange.Minimum;
                                if (Comparer.Default.Compare(res, argRange.Maximum) < 0) return argRange.Maximum;
                                return res;
                            
                            case LimitingRule.Loop:
                                return Utils.LoopIndex((int) res, (int) argRange.Minimum, (int) argRange.Maximum);
                            
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }
                }
                    
                return res;
                
            }).ToArray();
            
            _logger.LogInformation($"{commandInfo.Method.Name.Replace("Command", "")}({string.Join(",", args)})");
            
            if (args.Contains(null))
                return;
            
            commandInfo.Method.Invoke(commandInfo.ModuleInst, args);
        }
    }
}