using System;
using BioArtNet.CellExtension;
using BioArtNet.Core;
using BioArtNet.Example.Modules;
using BioArtNet.Tools.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace BioArtNet.Example
{
    public class World : IWorld<Environment, Interpreter>
    {
        public Environment Environment { get; }

        public Interpreter Interpreter { get; }

        public int Generation { get; protected set; }

        private readonly IServiceProvider _services;

        public World() : this(new ServiceCollection()) 
        { }
        
        public World(IServiceCollection serviceCollection)
        {
            ConfigureServices(serviceCollection);
            
            _services = serviceCollection.BuildServiceProvider();
            
            Environment = _services.GetRequiredService<Environment>();
            Interpreter = _services.GetRequiredService<Interpreter>();
        }

        private void ConfigureServices(IServiceCollection services)
        {
            services.Configure<EnvironmentOptions>(option =>
            {
                option.Size = new Vector2(32, 16);
            });

            services.AddSingleton(this);
            services.AddSingleton<Environment>();
            services.AddSingleton<Interpreter>();

            // Commands
            services.AddSingleton<CommandService<CommandContext>>();
            
            services.AddCommandModule<MoveModule, CommandContext>();
            services.AddCommandModule<ConditionModule, CommandContext>();
        }
        
        public void Iterate()
        {
            Interpreter.Process();
            
            Generation++;
        }
    }
}