using System.ComponentModel.DataAnnotations;
using BioArtNet.CellExtension;

namespace BioArtNet.Tools.CellExtension
{
    public static class Utils
    {
        public static int LoopIndex(int i, int n)
            => (i % n + n) % n;

        public static int LoopIndex(int i, int start, int end) 
            => LoopIndex(i - start, end - start) + start;

        /// <summary>
        /// 0 — up
        /// 1 — right
        /// 2 — down
        /// 3 — left
        /// </summary>
        /// <param name="dir"></param>
        /// <returns></returns>
        public static Vector2 VectorFromDir(byte dir)
        {
            switch (dir)
            {
                case 0: return Vector2.Up;
                case 1: return Vector2.Right;
                case 2: return Vector2.Down;
                case 3: return Vector2.Left;
                default: return Vector2.Zero;
            }
        }

        /// <summary>
        /// up    — 0
        /// right — 1
        /// down  — 2
        /// left  — 3
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static byte DirFromVector(Vector2 v)
        {
            if (v == Vector2.Up)
                return 0;
            if (v == Vector2.Right)
                return 1;
            if (v == Vector2.Down)
                return 2;
            if (v == Vector2.Left)
                return 3;

            return unchecked((byte) -1);
        }
    }
}