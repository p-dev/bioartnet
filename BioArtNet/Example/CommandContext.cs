using System.Collections.Generic;
using System.Drawing;
using BioArtNet.CellExtension;
using BioArtNet.Tools.CellExtension;

namespace BioArtNet.Example
{
    public class CommandContext : ICellEnvironmentCommandContext<Entity, Environment>
    {
        public Vector2 EntityPosition { get; }

        public Entity Entity { get; }

        public Environment Environment { get; }

        public CommandContext(Entity entity, Environment environment, Vector2 entityPos)
        {
            Entity = entity;
            Environment = environment;
            EntityPosition = entityPos;
        }
    }
}