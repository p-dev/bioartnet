using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using BioArtNet.Example;

namespace BioArtNet.Tools.Commands
{
    [AttributeUsage(AttributeTargets.Method)]
    public class CommandAttribute : Attribute
    {
        public byte OpCode { get; }
        public bool IsFinal { get; }
        
        public CommandAttribute(
            byte opCode,  
            bool isFinal = true)
        {
            OpCode = opCode;
            IsFinal = isFinal;
        }
        
        public CommandAttribute(
            OpCode opCode,  
            bool isFinal = true)
        {
            OpCode = (byte) opCode;
            IsFinal = isFinal;
        }
    }
}