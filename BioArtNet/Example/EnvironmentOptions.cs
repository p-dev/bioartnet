using BioArtNet.CellExtension;

namespace BioArtNet.Example
{
    public class EnvironmentOptions
    {
        public Vector2 Size { get; set; }
    }
}