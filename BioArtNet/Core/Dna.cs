using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BioArtNet.Tools.CellExtension;

namespace BioArtNet.Core
{
    public class Dna
    {
        public IReadOnlyList<byte> Code => new List<byte>(_bytes);
        private readonly byte[] _bytes;

        private ushort _codePtr;

        public ushort CodePtr
        {
            get => _codePtr;
            set => _codePtr = (ushort) Utils.LoopIndex(value, Code.Count);
        }
        
        /// <summary>
        /// Create a new DNA by given array of bytes.
        /// </summary>
        /// <param name="bytes">Array of bytes.</param>
        public Dna(byte[] bytes)
        {
            _bytes = bytes;
        }

        /// <summary>
        /// Generate random DNA by given length.
        /// </summary>
        /// <param name="length">Length of the DNA.</param>
        public Dna(int length)
        {
            var rand = new Random();
            _bytes = new byte[length];
            new Random().NextBytes(_bytes);
        }

        public static Dna FromString(string str)
        {
            var regex = new Regex(@"\s*{\s*([0-9A-Fa-f\s]+)\s*}\s*\[(\d+)\]");
            
            var gs = regex.Match(str).Groups;
            
            var bytes = gs[1].Value.Split(' ').Select(token => Convert.ToByte(token, 16)).ToArray();
            var ptr = ushort.Parse(gs[2].Value);

            var dna = new Dna(bytes)
            {
                CodePtr = ptr
            };
            
            return dna;
        }

        /// <example>
        /// { 04 3F 1A B0 10 }[3]
        /// </example>
        public override string ToString()
            => $"{{ {string.Join(" ", _bytes)} }}";
    }
}