using System.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using BioArtNet.CellExtension;
using BioArtNet.Commands;
using BioArtNet.Core;
using BioArtNet.Example.Modules;
using BioArtNet.Tools.CellExtension;
using BioArtNet.Tools.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace BioArtNet.Example
{
    public class Interpreter : IInterpreter<Environment>
    {
        public Environment Environment { get; }

        private readonly CommandService<CommandContext> _commandService;

        private readonly ILogger _logger;

        private readonly World _world;
        
        public Interpreter(
            ILogger<Interpreter> logger,
            Environment environment, 
            CommandService<CommandContext> commandService, World world)
        {
            _logger = logger;
            _commandService = commandService;
            _world = world;
            Environment = environment;
        }

        public void Process()
        {
            using (_logger.BeginScope($"Generation {_world.Generation}"))
            {
                var alreadyProcessedEntities = new HashSet<Entity>();
                
                for (var i = 0; i < Environment.Height; i++) 
                for (var j = 0; j < Environment.Width; j++)
                {
                    if (!Environment[j, i].HasEntity()) continue;
                    
                    label_1:
                    
                    var entityPos = new Vector2(j, i);
                    var entity = Environment[entityPos].Entity;
                    
                    // Skip if entity has been already processed
                    if (alreadyProcessedEntities.Contains(entity)) continue;

                    var opCode = entity.Dna.GetByte();
                    
                    var commandInfo = _commandService.Commands[opCode];

                    var context = new CommandContext(entity, Environment, entityPos);
                    
//                    commandInfo.Invoke(context);
                    _commandService.InvokeCommand(opCode, context);
                    
                    if (!commandInfo.IsFinal) goto label_1;
                    
                    alreadyProcessedEntities.Add(entity);
                }

                foreach (var entity in Environment.Entities)
                {
//                    _logger.LogInformation($"CodePtr: {entity.CodePointer}");
                }
            }
        }
    }

    internal class InterpretingException : Exception
    {
        public InterpretingException(string message, Exception innerException)
            : base(message, innerException)
        {
            
        }
    }

}