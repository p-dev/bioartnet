using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using BioArtNet.Commands;
using BioArtNet.Core;
using BioArtNet.Example;
using BioArtNet.Tools.CellExtension;

namespace BioArtNet.Tools.Commands
{
    public class CommandInfo<TCommandContext>
        where TCommandContext : ICommandContext<IEntity>
    {
        public int OpCode { get; }
        
        public bool IsFinal { get; }
        
        public MethodInfo Method { get; }

        public ModuleBase<TCommandContext> ModuleInst { get; }

        public IReadOnlyList<ArgumentInfo> ArgInfos { get; }
        
        public CommandInfo(MethodInfo method, ModuleBase<TCommandContext> moduleInst, CommandAttribute commandAttribute)
        {
            Method = method;
            ModuleInst = moduleInst;
            OpCode = commandAttribute.OpCode;
            IsFinal = commandAttribute.IsFinal;
            ArgInfos = Method.GetParameters().Select(p => new ArgumentInfo(
                p.ParameterType, 
                p.GetCustomAttribute<ArgRangeAttribute>())
            ).ToArray();
        }
    }
}