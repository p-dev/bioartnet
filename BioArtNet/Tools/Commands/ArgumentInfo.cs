using System;
using System.ComponentModel.DataAnnotations;

namespace BioArtNet.Tools.Commands
{
    public class ArgumentInfo
    {
        public Type Type { get; }

        public ArgRangeAttribute ArgRange { get; }
        
        public ArgumentInfo(Type type, ArgRangeAttribute argRange)
        {
            Type = type;
            ArgRange = argRange;
        }
    }
}