using System;
using System.IO;
using System.Linq;

namespace BioArtNet.Core
{
    public static class DnaExtensions
    {
        public static byte GetByte(this Dna dna)
        {
            var result = dna.Code[dna.CodePtr];
            dna.CodePtr += 1;
            return result;
        }
        
        public static sbyte GetSByte(this Dna dna)
        {
            var result = (sbyte) dna.Code[dna.CodePtr];
            dna.CodePtr += 1;
            return result;
        }
        
        public static short GetShort(this Dna dna)
        {
            var result = BitConverter.ToInt16(dna.Code.ToArray(), dna.CodePtr);
            dna.CodePtr += 2;
            return result;
        }
        
        public static ushort GetUShort(this Dna dna)
        {
            var result = BitConverter.ToUInt16(dna.Code.ToArray(), dna.CodePtr);
            dna.CodePtr += 2;
            return result;
        }
        
        public static int GetInt(this Dna dna)
        {
            var result = BitConverter.ToInt32(dna.Code.ToArray(), dna.CodePtr);
            dna.CodePtr += 4;
            return result;
        }
        
        public static uint GetUInt(this Dna dna)
        {
            var result = BitConverter.ToUInt32(dna.Code.ToArray(), dna.CodePtr);
            dna.CodePtr += 4;
            return result;
        }
        
        public static long GetLong(this Dna dna)
        {
            var result = BitConverter.ToInt64(dna.Code.ToArray(), dna.CodePtr);
            dna.CodePtr += 8;
            return result;
        }
        
        public static ulong GetULong(this Dna dna)
        {
            var result = BitConverter.ToUInt64(dna.Code.ToArray(), dna.CodePtr);
            dna.CodePtr += 8;
            return result;
        }
        
        public static Dna Mutate(this Dna dna, float p)
        {
            var newCode = new byte[dna.Code.Count];
            var rand = new Random();
            for (var i = 0; i < newCode.Length; i++)
            {
                if (rand.NextDouble() <= p)
                    newCode[i] = (byte) rand.Next();
                else
                    newCode[i] = dna.Code[i];
            }

            var newDna = new Dna(newCode);
            return newDna;
        }
    }
}