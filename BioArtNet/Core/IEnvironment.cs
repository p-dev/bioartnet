using System.Collections.Generic;

namespace BioArtNet.Core
{
    /// <summary>
    /// Окружение.
    /// </summary>
    /// <typeparam name="TEntity">Тип сущностей в окруении.</typeparam>
    public interface IEnvironment<out TEntity>
        where TEntity : IEntity
    {
        /// <summary>
        /// Список всех сущностей в этом окружении.
        /// </summary>
        IReadOnlyCollection<TEntity> Entities { get; }
    }
}