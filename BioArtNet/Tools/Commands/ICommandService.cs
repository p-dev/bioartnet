using BioArtNet.Commands;
using BioArtNet.Core;

namespace BioArtNet.Tools.Commands
{
    public interface ICommandService<TCommandContext>
        where TCommandContext : ICommandContext<IEntity>
    {
        
    }
}