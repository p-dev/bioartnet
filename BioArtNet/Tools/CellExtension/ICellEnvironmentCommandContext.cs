using BioArtNet.CellExtension;
using BioArtNet.Commands;
using BioArtNet.Core;
using BioArtNet.Tools.Commands;

namespace BioArtNet.Tools.CellExtension
{
    public interface ICellEnvironmentCommandContext<out TEntity, out TCellEnvironment> : IEnvironmentCommandContext<TEntity, TCellEnvironment>
        where TCellEnvironment : ICellEnvironment<ICellEntity, ICell<IEntity>>
        where TEntity : ICellEntity
    {
        Vector2 EntityPosition { get; }
    }
}