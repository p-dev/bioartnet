namespace BioArtNet.Core
{
    /// <summary>
    /// Визуализатор. Визуализирует.
    /// </summary>
    /// <typeparam name="TWorld">Тип мира.</typeparam>
    public interface IVisualizer<out TWorld>
        where TWorld : IWorld<IEnvironment<IEntity>, IInterpreter<IEnvironment<IEntity>>>
    {
        /// <summary>
        /// Мир, который будет визуализироваться.
        /// </summary>
        TWorld World { get; }
        
        /// <summary>
        /// Процедура, которая будет визуализировать.
        /// </summary>
        void Render();
    }
}