using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Threading;
using BioArtNet.Commands;
using BioArtNet.Core;
using BioArtNet.Example;
using BioArtNet.Example.Modules;
using BioArtNet.Tools.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Gif;
using SixLabors.ImageSharp.PixelFormats;
using Xunit;
using Xunit.Abstractions;

namespace BioArtNetTest
{
    public class WorldAllTest
    {
        private readonly ITestOutputHelper _out;

        public WorldAllTest(ITestOutputHelper @out)
        {
            _out = @out;
        }

        [Fact]
        public void WorldTest()
        {
            var services = new ServiceCollection();
            services.AddLogging(sp => sp.AddXunit(_out));
            
            var world = new World(services);
            
            var visualizer = new Visualizer(world, "../../../Generations");
            
            visualizer.Render();
            for (var i = 0; i < 16; i++)
            {
                world.Iterate();
                visualizer.Render();
            }
//            visualizer.GenGif();
        }

        [Fact]
        public void GifTest()
        {
            using (var gifImage = new Image<Rgba32>(128, 64))
            {
                var rand = new Random();
                for (var n = 0; n < 16; n++)
                {
                    using (var frameImage = new Image<Rgba32>(128, 64))
                    {
                        for (var i = 0; i < 128; i++)
                            for (var j = 0; j < 64; j++)
                            {
                                frameImage[i, j] = new Rgba32(
                                    (float) rand.NextDouble(),
                                    (float) rand.NextDouble(),
                                    (float) rand.NextDouble()
                                );
                            }
                        gifImage.Frames.InsertFrame(n, frameImage.Frames.RootFrame);
                    }
                }

                gifImage.SaveAsGif(new FileStream("../../../test.gif", FileMode.Create));
            }
        }
    }
}