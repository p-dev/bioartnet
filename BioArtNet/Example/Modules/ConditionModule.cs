using System;
using BioArtNet.Tools.CellExtension;
using BioArtNet.Tools.Commands;

namespace BioArtNet.Example.Modules
{
    public class ConditionModule : ModuleBase<CommandContext>
    {
        [Command(OpCode.Goto, false)]
        public void GotoCommand(byte cmdPtr)
        {
            Goto2Command(cmdPtr);
        }

        [Command(OpCode.Goto2, false)]
        public void Goto2Command(ushort cmdPtr)
        {
            Context.Entity.Dna.CodePtr = cmdPtr;
        }

        [Command(OpCode.If, false)]
        public void IfCommand(byte ptr)
        {
            If2Command(ptr);
        }

        [Command(OpCode.If2, false)]
        public void If2Command(ushort ptr)
        {
            if (Context.Entity.Stack.Pop() != 0)
            {
                Goto2Command(ptr);
            }
        }

        [Command(OpCode.CanMove, false)]
        public void CanMoveCommand()
        {
            var result = Context.Environment[Context.EntityPosition + Context.Entity.Orientation.ToVector2()].IsPassable;
            Context.Entity.Stack.Push(Convert.ToByte(result));
        }
    }
}