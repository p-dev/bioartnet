using BioArtNet.Commands;
using BioArtNet.Core;

namespace BioArtNet.Tools.Commands
{
    public abstract class ModuleBase<TContext>
        where TContext : ICommandContext<IEntity>
    {
        public TContext Context { get; internal set; }
    }
}