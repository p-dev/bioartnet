using System;
using System.Collections.Generic;
using BioArtNet.Core;
using BioArtNet.Tools.CellExtension;
using static BioArtNet.Tools.CellExtension.Utils;

namespace BioArtNet.Example
{
    public class Entity : ICellEntity
    {
        public Dna Dna { get; }

        public Stack<byte> Stack { get; }

        public IDictionary<int, byte> Memory { get; }

        public Direction Orientation { get; set; } = Direction.Up;

        public byte Energy { get; set; }

        public Entity()
        {
            Dna = new Dna(64);
            
            Memory = new Dictionary<int, byte>();
            Stack = new Stack<byte>();
        }

        public Entity(Dna dna)
        {
            Dna = dna;
            
            Memory = new Dictionary<int, byte>();
            Stack = new Stack<byte>();
        }
    }
}