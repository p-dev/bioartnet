namespace BioArtNet.CellExtension
{
    public class Vector2
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Vector2()
        {
            X = default;
            Y = default;
        }

        public Vector2(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static Vector2 Zero => new Vector2(0, 0);
        public static Vector2 One => new Vector2(1, 1);
        public static Vector2 Up => new Vector2(0, 1);
        public static Vector2 Right => new Vector2(1, 0);
        public static Vector2 Down => new Vector2(0, -1);
        public static Vector2 Left => new Vector2(-1, 0);

        public static Vector2 operator +(Vector2 lv, Vector2 rv) 
            => new Vector2(lv.X + rv.X, lv.Y + rv.Y);

        public static Vector2 operator -(Vector2 lv, Vector2 rv) 
            => new Vector2(lv.X - rv.X, lv.Y - rv.Y);

        public static bool operator ==(Vector2 lv, Vector2 rv)
            => lv.X == rv.X && 
               lv.Y == rv.Y;

        public static bool operator !=(Vector2 lv, Vector2 rv) => !(lv == rv);

        public override bool Equals(object obj)
        {
            if (obj is Vector2 vec)
                return this == vec;
            else
                return false;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X * 397) ^ Y;
            }
        }

        public override string ToString()
            => $"({X}; {Y})";
    }
}