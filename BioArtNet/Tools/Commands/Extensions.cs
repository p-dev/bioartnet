using System;
using System.Reflection;
using BioArtNet.Commands;
using BioArtNet.Core;
using BioArtNet.Example.Modules;
using Microsoft.Extensions.DependencyInjection;

namespace BioArtNet.Tools.Commands
{
    public static class Extensions
    {
        public static IServiceCollection AddCommandModule<TModule, TCommandContext>(this IServiceCollection services)
            where TModule : ModuleBase<TCommandContext>
            where TCommandContext : ICommandContext<IEntity>
            => services.AddCommandModule<TCommandContext>(typeof(TModule));

        public static IServiceCollection AddCommandModule<TContext>(this IServiceCollection services, Type moduleType)
            where TContext : ICommandContext<IEntity>
        {
            services.Add(new ServiceDescriptor(
                typeof(ModuleBase<TContext>), 
                moduleType, 
                ServiceLifetime.Singleton));
            return services;
        }
    }
}