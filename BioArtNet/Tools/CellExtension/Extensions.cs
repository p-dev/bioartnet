using System;
using BioArtNet.CellExtension;
using BioArtNet.Core;

namespace BioArtNet.Tools.CellExtension
{
    public static class Extensions
    {
        public static bool HasEntity<TEntity>(this ICell<TEntity> cell)
            where TEntity : IEntity 
            => cell.Entity != null;

        public static bool TryMoveEntity<TEntity>(this ICellEnvironment<TEntity, ICell<TEntity>> cellEnvironment, Vector2 from, Vector2 to)
            where TEntity : class, IEntity
        {
            if (!cellEnvironment[from].HasEntity() ||
                !cellEnvironment[to].IsPassable)
                return false;
            
            var entity = cellEnvironment[from].Entity;
            cellEnvironment[from].SetEntity(null);
            cellEnvironment[to].SetEntity(entity);

            return true;
        }

        public static Vector2 ToVector2(this Direction orientation)
        {
            switch (orientation)
            {
                case Direction.Up: return Vector2.Up;
                case Direction.Right: return Vector2.Right;
                case Direction.Down: return Vector2.Down;
                case Direction.Left: return Vector2.Left;
                default:
                    throw new ArgumentOutOfRangeException(nameof(orientation), orientation, null);
            }
        }

        public static Direction ToOrientation(this Vector2 vec)
        {
            if (vec == Vector2.Up)
                return Direction.Up;
            if (vec == Vector2.Right)
                return Direction.Right;
            if (vec == Vector2.Down)
                return Direction.Down;
            if (vec == Vector2.Left)
                return Direction.Left;

            return (Direction) 0;
        }
    }
}