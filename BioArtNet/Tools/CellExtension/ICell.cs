using BioArtNet.Core;

namespace BioArtNet.Tools.CellExtension
{
    public interface ICell<out TEntity> 
        where TEntity : IEntity
    {
        TEntity Entity { get; }
        
        bool IsPassable { get; }
        
        void SetEntity(IEntity entity);
    }
}