using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using BioArtNet.Core;
using BioArtNet.CellExtension;
using BioArtNet.Tools.CellExtension;
using Microsoft.Extensions.Options;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using static BioArtNet.Tools.CellExtension.Utils;

namespace BioArtNet.Example
{
    public class Environment : ICellEnvironment<Entity, Cell>
    {
        public IReadOnlyCollection<Entity> Entities
            => _cells.Cast<Cell>().Where(c => c.HasEntity()).Select(c => c.Entity).ToList();

        private readonly Cell[,] _cells;

        public int Width { get; }
        public int Height { get; }

        public Cell this[int x, int y]
        {
            get => _cells[LoopIndex(y, Height), LoopIndex(x, Width)];
            set => _cells[LoopIndex(y, Height), LoopIndex(x, Width)] = value;
        }

        public Cell this[Vector2 v]
        {
            get => this[v.X, v.Y];
            set => this[v.X, v.Y] = value;
        }

        public Environment(IOptions<EnvironmentOptions> options)
        {
            Width = options.Value.Size.X;
            Height = options.Value.Size.Y;
            
            _cells = new Cell[Height, Width];

            var rand = new Random();
            
            LoadMapFromFile("/home/prunkles/Develop/cs/Projects/BioArtNet/BioArtNetTest/map.txt");
            
            this[17, 4].Entity = new Entity(new Dna(new byte[]
            {
                0x30, // CanMove
                0x12, 0x07, // If
                0x04, 0x01, // Else, Rotate
                0x10, 0x00, // Goto
                0x01, // Move
            }));
        }
        
        private void LoadMapFromFile(string path)
        {
            var lines = File.ReadAllLines(path);
            
            var width = lines[0].Length;
            var height = lines.Length;
            
            if (lines.Any(l => l.Length != width)) 
                throw new ArgumentOutOfRangeException("", "The map must have a rectangle size.");

            for (var i = 0; i < height; i++)
            for (var j = 0; j < width; j++)
            {
                switch (lines[i][j])
                {
                    case '#':
                        this[j, i] = new Cell()
                        {
                            IsWall = true,
                        };
                        break;
                    
                    default:
                        this[j, i] = new Cell();
                        break;
                }
            }
        }
    }
}