using System;
using System.ComponentModel.DataAnnotations;

namespace BioArtNet.Tools.Commands
{
    public class ArgRangeAttribute : RangeAttribute
    {
        public LimitingRule LimitingRule { get; }
        
        public ArgRangeAttribute(double minimum, double maximum, LimitingRule limitingRule) : base(minimum, maximum)
        {
            LimitingRule = limitingRule;
        }

        public ArgRangeAttribute(int minimum, int maximum, LimitingRule limitingRule) : base(minimum, maximum)
        {
            LimitingRule = limitingRule;
        }

        public ArgRangeAttribute(Type type, string minimum, string maximum, LimitingRule limitingRule) : base(type, minimum, maximum)
        {
            LimitingRule = limitingRule;
        }
    }
    
    public enum LimitingRule
    {
        DefaultValue,
        Constrain,
        Loop,
        Ignore,
    }
}