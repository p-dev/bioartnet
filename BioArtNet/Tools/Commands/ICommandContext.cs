using System.Collections.Generic;
using BioArtNet.Core;

namespace BioArtNet.Commands
{
    public interface ICommandContext<out TEntity>
        where TEntity : IEntity
    {
        TEntity Entity { get; }
    }
}