using BioArtNet.Commands;
using BioArtNet.Core;

namespace BioArtNet.Tools.Commands
{
    public interface IEnvironmentCommandContext<out TEntity, out TEnvironment> : ICommandContext<TEntity>
        where TEnvironment : IEnvironment<IEntity>
        where TEntity : IEntity
    {
        TEnvironment Environment { get; }
    }
}