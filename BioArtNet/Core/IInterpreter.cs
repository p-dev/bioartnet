using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace BioArtNet.Core
{
    /// <summary>
    /// Интерпретатор. Исполняет команды в сущностях, взаимодействую с окружающей средой.
    /// </summary>
    /// <typeparam name="TEntity">Тип сущностей.</typeparam>
    /// <typeparam name="TEnvironment">Тип окружающей среды.</typeparam>
    public interface IInterpreter<out TEnvironment>
        where TEnvironment : IEnvironment<IEntity>
    {
        /// <summary>
        /// Окружающая среда, в которой работает интерпретатор.
        /// </summary>
        TEnvironment Environment { get; }

        /// <summary>
        /// Реализуется вызовом команд у всех сущностей.
        /// </summary>
        void Process();
    }
}